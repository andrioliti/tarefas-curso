package com.amandrioli.servidor.tarefas.clientes.service.v1;

import org.springframework.stereotype.Service;

import com.amandrioli.servidor.tarefas.abstracts.AbstractService;
import com.amandrioli.servidor.tarefas.clientes.model.Cliente;
import com.amandrioli.servidor.tarefas.clientes.repository.ClienteRepo;

@Service
public class ClienteServiceV1 
	extends AbstractService<Cliente, ClienteRepo>{
	
}
