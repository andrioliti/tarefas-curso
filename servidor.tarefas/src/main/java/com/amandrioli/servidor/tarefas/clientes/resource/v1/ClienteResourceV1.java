package com.amandrioli.servidor.tarefas.clientes.resource.v1;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.amandrioli.servidor.tarefas.clientes.model.Cliente;
import com.amandrioli.servidor.tarefas.clientes.repository.ClienteRepo;
import com.amandrioli.servidor.tarefas.clientes.service.v1.ClienteServiceV1;
import com.amandrioli.servidor.tarefas.tarefas.model.Tarefa;
import com.amandrioli.servidor.tarefas.utils.ObjectUtils;

import javassist.NotFoundException;

@RestController
@RequestMapping(path = "clientes")
public class ClienteResourceV1 {
	
	@Autowired
	private ClienteServiceV1 clienteService;

	
	@GetMapping
	public List<Cliente> getAll() {
		return this.clienteService.getAll();
	}
	
	@PostMapping
	public Cliente create(
			@RequestBody Cliente cliente) {
		return this.clienteService.save(cliente);
	}
	
	@PutMapping
	public Cliente update(
			@RequestBody Cliente cliente) throws Exception {
		
		Optional<Cliente> findOne 
			= this.clienteService.findOne(cliente.getCodigo());
		
		if (findOne.isEmpty()) {
			throw new NotFoundException("Entidade não encontrada");
		}
	
		Cliente forUpdate = findOne.get();
		
		ObjectUtils.merge(cliente, forUpdate);
		
		return this.clienteService.save(forUpdate);
	}
	
	@DeleteMapping
	public void delete(
			@RequestParam("codigo") Integer id) {
		this.clienteService.delete(id);
	}
}
