package com.amandrioli.servidor.tarefas.tarefas.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import org.hibernate.annotations.ColumnDefault;

import com.amandrioli.servidor.tarefas.clientes.model.Cliente;
import com.fasterxml.jackson.annotation.JsonFormat;

@Entity(name = "tarefa")
@SequenceGenerator(name = "tarefa_sequence_generator")
public class Tarefa {
	@Id
	@GeneratedValue(
		strategy = GenerationType.SEQUENCE, 
		generator = "tarefa_sequence_generator")
	private Integer codigo;
	private String descricao;
	private String titulo;
	@Column(name = "data_criacao")
	@JsonFormat
    	(shape = JsonFormat.Shape.STRING, pattern = "dd/mm/yyyy")
	private Date dataCriacao;
	@Column(name = "data_final")
	@JsonFormat
    	(shape = JsonFormat.Shape.STRING, pattern = "dd/mm/yyyy")	
	private Date dataFinal;
	@Enumerated(EnumType.STRING)
	private EstadoTarefa estado;
	@ColumnDefault(value = "0")
	private Integer criticidade;

	public Integer getCodigo() {
		return codigo;
	}

	public void setCodigo(Integer codigo) {
		this.codigo = codigo;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public Date getDataCriacao() {
		return dataCriacao;
	}

	public void setDataCriacao(Date dataCriacao) {
		this.dataCriacao = dataCriacao;
	}

	public Date getDataFinal() {
		return dataFinal;
	}

	public void setDataFinal(Date dataFinal) {
		this.dataFinal = dataFinal;
	}

	public EstadoTarefa getEstado() {
		return estado;
	}

	public void setEstado(EstadoTarefa estado) {
		this.estado = estado;
	}

	public Integer getCriticidade() {
		return criticidade;
	}

	public void setCriticidade(Integer criticidade) {
		this.criticidade = criticidade;
	}

}
