package com.amandrioli.servidor.tarefas.clientes.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;

import com.amandrioli.servidor.tarefas.tarefas.model.Tarefa;

@Entity(name = "cliente")
@SequenceGenerator(name = "cliente_sequence_generator")
public class Cliente {
	@Id
	@GeneratedValue(
			strategy = GenerationType.SEQUENCE,
			generator = "cliente_sequence_generator")
	private Integer codigo;
	private String nome;
	private String login;
	private String senha;
	private String email;
	
	@Column(name = "codigo_verificacao")
	private String codigoVerificacao;
	
	@OneToMany(fetch = FetchType.LAZY)
	@JoinColumn(name = "cliente_codigo")
	private List<Tarefa> tarefas;
	
	public List<Tarefa> getTarefas() {
		return tarefas;
	}

	public void setTarefas(List<Tarefa> tarefas) {
		this.tarefas = tarefas;
	}

	public Integer getCodigo() {
		return codigo;
	}

	public void setCodigo(Integer codigo) {
		this.codigo = codigo;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getCodigoVerificacao() {
		return codigoVerificacao;
	}

	public void setCodigoVerificacao(String codigoVerificacao) {
		this.codigoVerificacao = codigoVerificacao;
	}

}
