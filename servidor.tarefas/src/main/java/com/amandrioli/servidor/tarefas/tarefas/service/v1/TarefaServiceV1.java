package com.amandrioli.servidor.tarefas.tarefas.service.v1;

import org.springframework.stereotype.Service;

import com.amandrioli.servidor.tarefas.abstracts.AbstractService;
import com.amandrioli.servidor.tarefas.tarefas.model.Tarefa;
import com.amandrioli.servidor.tarefas.tarefas.repository.TarefaRepository;

@Service
public class TarefaServiceV1 extends AbstractService<Tarefa, TarefaRepository>{

}
