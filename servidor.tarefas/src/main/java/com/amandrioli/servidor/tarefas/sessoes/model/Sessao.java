package com.amandrioli.servidor.tarefas.sessoes.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import com.amandrioli.servidor.tarefas.clientes.model.Cliente;

@Entity(name = "sessao")
public class Sessao {
	@Id
	private String token;
	private Date iat;
	private Date exp;
	
	@ManyToOne(fetch = FetchType.LAZY)
	private Cliente cliente;

	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public Date getIat() {
		return iat;
	}

	public void setIat(Date iat) {
		this.iat = iat;
	}

	public Date getExp() {
		return exp;
	}

	public void setExp(Date exp) {
		this.exp = exp;
	}

}
