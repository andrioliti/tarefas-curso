package com.amandrioli.servidor.tarefas.tarefas.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.amandrioli.servidor.tarefas.tarefas.model.Tarefa;

public interface TarefaRepository extends JpaRepository<Tarefa, Integer>{

}
