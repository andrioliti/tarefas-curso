package com.amandrioli.servidor.tarefas.utils;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public abstract class ObjectUtils {
	private static String GET = "get";
	private static String SET = "set";
	
	public static void merge(Object from, Object to) throws Exception {
		Class<? extends Object> fromClass = from.getClass();
		Class<? extends Object> toClass = to.getClass();
		
		if (!fromClass.equals(toClass))
			throw new Exception("Classes não são iguais");
		
		for (Method methodGet: fromClass.getMethods()) {
			String nameFrom = getAttributeName(methodGet);
			String prefixFrom = getPrefix(methodGet);
			
			if (!prefixFrom.equals(GET))
				continue;
			
			for (Method methodSet: toClass.getMethods()) {
				String nameTo = getAttributeName(methodSet);
				String prefixTo = getPrefix(methodSet);
				
				if (!prefixTo.equals(SET))
					continue;
				
				if (nameFrom.equals(nameTo)) {
					try {
						Object invokeRes = methodGet.invoke(from);
						
						if (invokeRes != null)
							methodSet.invoke(to, invokeRes);
						
					} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
						e.printStackTrace();
					}
				}
			}
		}
		
	}
	
	private static String getPrefix(Method method) {
		return method.getName().substring(0,3);
	}
	
	private static String getAttributeName(Method method) {
		return method.getName().substring(3);
	}
	
}
