package com.amandrioli.servidor.tarefas.tarefas.resource.v1;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.amandrioli.servidor.tarefas.tarefas.model.Tarefa;
import com.amandrioli.servidor.tarefas.tarefas.service.v1.TarefaServiceV1;
import com.amandrioli.servidor.tarefas.utils.ObjectUtils;

import javassist.NotFoundException;

@RestController
@RequestMapping(path = "tarefas")
public class TarefaResourceV1 {
	
	@Autowired
	private TarefaServiceV1 tarefaService;

	
	@GetMapping
	public List<Tarefa> getAll() {
		return this.tarefaService.getAll();
	}
	
	@PostMapping
	public Tarefa create(
			@RequestBody Tarefa Tarefa) {
		return this.tarefaService.save(Tarefa);
	}
	
	@PutMapping
	public Tarefa update(
			@RequestBody Tarefa tarefa) throws Exception {
		Optional<Tarefa> findOne 
			= this.tarefaService.findOne(tarefa.getCodigo());
		
		if (findOne.isEmpty()) {
			throw new NotFoundException("Entidade não encontrada");
		}
		
		Tarefa t = findOne.get();
		
		ObjectUtils.merge(tarefa, t);
			
		return this.tarefaService.save(t);
	}
	
	@DeleteMapping
	public void delete(
			@RequestParam("codigo") Integer id) {
		this.tarefaService.delete(id);
	}
}
