package com.amandrioli.servidor.tarefas.tarefas.model;

public enum EstadoTarefa {
	AGUARDANDO, EXECUTANDO, FEITO;
}
