package com.amandrioli.servidor.tarefas.clientes.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.amandrioli.servidor.tarefas.clientes.model.Cliente;

public interface ClienteRepo extends JpaRepository<Cliente, Integer> {

}
