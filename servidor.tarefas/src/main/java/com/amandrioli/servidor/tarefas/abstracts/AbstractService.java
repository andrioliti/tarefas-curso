package com.amandrioli.servidor.tarefas.abstracts;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;

public class AbstractService<M, R extends JpaRepository<M, Integer>> {
	
	@Autowired
	protected R repo;
	
	public List<M> getAll() {
		return repo.findAll();
	}
	
	public Optional<M> findOne(Integer codigo) {
		return repo.findById(codigo);
	}
	
	public void delete(Integer id) {
		repo.deleteById(id);
	}
	
	public M save(M entity) {
		return repo.save(entity);
	}
	
}
