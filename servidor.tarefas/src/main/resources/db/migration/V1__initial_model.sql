drop table if exists cliente cascade;
drop table if exists sessao cascade;
drop table if exists tarefa cascade;

drop sequence if exists cliente_sequence_generator;
drop sequence if exists tarefa_sequence_generator;

create sequence cliente_sequence_generator start 1 increment 50;
create sequence tarefa_sequence_generator start 1 increment 50;

create table cliente (
  codigo int4 not null,
  codigo_verificacao varchar(255),
  email varchar(255),
  login varchar(255),
  nome varchar(255),
  senha varchar(255),
  primary key (codigo)
);

create table sessao (
  token varchar(255) not null,
  exp timestamp,
  iat timestamp,
  cliente_codigo int4,
  primary key (token)
);

create table tarefa (
  codigo int4 not null,
  criticidade int4 default 0,
  data_criacao timestamp,
  data_final timestamp,
  descricao varchar(255),
  estado varchar(255),
  titulo varchar(255),
  cliente_codigo int4,
  primary key (codigo)
);

alter table sessao 
   add constraint sessao_cliente_constraint 
   foreign key (cliente_codigo) 
   references cliente;

alter table tarefa 
   add constraint tarefa_cliente_constraint 
   foreign key (cliente_codigo) 
   references cliente;